$(function() {

  var socket = io.connect("http://localhost:3000")

  var chains = {}

  function reportEvent(socket, handlerHash, e) {
    e.preventDefault();
    var eventInfo = R.omit(e.originalEvent, ["currentTarget", "srcElement", "target", "toElement", "view", "fromElement", "relatedTarget"])
    socket.emit("event", {handler: handlerHash, info: eventInfo})
  }

  socket.on("new-command-chain", function(id) {
    chains[id] = true
  })

  socket.on("command", function(data) {
    var id = data.chain,
        command = data.command,
        params = data.params,
        chain = chains[id]
    if (!chain) { return }
    console.log(" * New command:", data)
    switch (command) {
      case "$":
        chain = $.apply($, params)
        break
      case "on":
        if (data.scope) {
          chain = chain.on(params.event, data.scope, reportEvent.bind({}, socket, params.handler))
        } else {
          chain = chain.on(params.event, reportEvent.bind({}, socket, params.handler))
        }
        break
      default:
        chain = chain[command].apply(chain, params)
    }
    chains[id] = chain
  })

  socket.on("query", function(data) {
    var id = data.chain,
        command = data.command,
        params = data.params,
        stamp = data.stamp,
        chain = chains[id]
    if (!chain) { return }
    console.log(" * Query:", data)
    var value = chain[command].apply(chain, params),
        responseName = id+"-"+command+"-"+stamp
    socket.emit(responseName, value)
  })

})
