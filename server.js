var Q = require("q"),
    $ = require("./fakeQuery")

$(function($) {

  var button = $("#button"),
      output = $("#output")

  button.on("click", function() {
    output.html("<p>Ok!</p>");
  })

  output.on("mouseover", function() {
    output.text("get out!")
    output.css("backgroundColor", "red")
  })
  .on("mouseout", function() {
    output.text("Click on the button!")
    output.css("backgroundColor", "grey")
    console.log("wait for it...")
    button.html().then(function(html) {
      output.html("Hey, I read '" + html + "' up there!")
    })
  })

})
