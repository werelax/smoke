var Q = require("q")
  , express = require("express")
  , app = express()
  , server = require("http").createServer(app)
  , io = require("socket.io").listen(server)

/* Serving static files */

io.set("log level", 1)

app.configure(function() {
  app.use(express.bodyParser())
  app.use(express.cookieParser("Oh my!"))
  app.use(express.session())
  app.use(app.router)
  app.use(express.static(__dirname + "/public"))
})

server.listen(3000)

/* Query comamnd */

function randString(len) {
  return Math.floor(Math.random() * Math.pow(36, len)).toString(36)
}

function QueryCommandChain(socket) {
  this.socket = socket;
  this.queue = [];
  this.id = randString(128)
  this.socket.emit("new-command-chain", this.id)
}
QueryCommandChain.prototype = {
  send: function(command, params) {
    this.socket.emit("command", {
      chain: this.id,
      command: command,
      params: params
    })
  },
  on: function(event, scope, handler) {
    handler || ((handler = scope) && (scope = undefined))
    var hash = generateCallbackContinuation(handler, this.socket)
    this.send("on", {event: event, scope: scope, handler: hash})
    return this;
  },
  /* PENDING:
    "on"
    "one"
    "off"
  */
  queries0: [ "val", "text", "html" ],
  queries1: [ "hasClass", "prop", "attr",  "css" ],
  commands: [ "first", "last", "queue", "dequeue", "delay", "clearQueue", "attr", "removeAttr", "prop", "removeProp", "addClass", "removeClass", "toggleClass", "find", "closest", "add", "addBack", "parent", "parents", "parentsUntil", "next", "prev", "nextAll", "prevAll", "nextUntil", "prevUntil", "siblings", "children", "contents", "text", "append", "prepend", "before", "after", "remove", "empty", "clone", "html", "replaceWith", "detach", "appendTo", "prependTo", "insertBefore", "insertAfter", "replaceAll", "wrapAll", "wrapInner", "wrap", "unwrap", "css", "show", "hide", "toggle", "fadeTo", "animate", "stop", "finish", "slideDown", "slideUp", "slideToggle", "fadeIn", "fadeOut", "fadeToggle" ]
}

QueryCommandChain.prototype.queries0
.concat(QueryCommandChain.prototype.queries1)
.forEach(function(q) {
  QueryCommandChain.prototype[q] = function() {
    var defer = Q.defer(),
        stamp = Date.now(),
        responseName = this.id+"-"+q+"-"+stamp
    this.socket.emit("query", {
      chain: this.id,
      command: q,
      stamp: stamp,
      params: [].slice.call(arguments)
    })
    this.socket.once(responseName, function(value) {
      defer.resolve(value);
    })
    return defer.promise
  }
})

QueryCommandChain.prototype.commands.forEach(function(cmd) {
  var q = undefined
  if (QueryCommandChain.prototype.queries0.indexOf(cmd) != -1) {
    q = 0
    QueryCommandChain.prototype[cmd+"_q0"] = QueryCommandChain.prototype[cmd]
  }
  if (QueryCommandChain.prototype.queries1.indexOf(cmd) != -1) {
    q = 1
    QueryCommandChain.prototype[cmd+"_q1"] = QueryCommandChain.prototype[cmd]
  }
  QueryCommandChain.prototype[cmd] = function() {
    var args = [].slice.call(arguments)
    if (q === 0 && args.length === 0) { return this[cmd+"_q0"]() }
    if (q === 1 && args.length === 1) { return this[cmd+"_q1"](args[0]) }
    this.send(cmd, args);
    return this;
  }
})

/* Handler continuations */

var handlers = {};

function generateCallbackContinuation(handler, socket) {
  var hash = randString(32)
  handlers[hash] = handler
  return hash
}

/* Interface */

var $ = function(socket) {
  var socket = socket;
  return function() {
    var chain = new QueryCommandChain(socket)
    chain.send("$", [].slice.call(arguments))
    return chain;
  }
}

/* Socket logic */

io.sockets.on("connection", function(socket) {
  socket.on("event", function(eventData) {
    var handler = handlers[eventData.handler]
    if (!handler) { return }
    handler(eventData)
  })

  $.init($(socket))
})

/* Stupid export facade */

var initialization = function(init) {
  $.init = init;
}

module.exports = initialization;
